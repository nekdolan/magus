const express = require('express');
const bodyParser = require('body-parser');
const join = require('path').join;
const app = express();

app.use(bodyParser.json());
app.use(express.static(join(__dirname,'../client/static/')));
app.set('views', './client/views');
app.set('view engine', 'pug');

app.get('/', function (req, res){
  res.render('index', {});
});

function start(serverPort, cb){
  let port = serverPort || 80;
  app.listen(port, function(){
    console.info('App started on port: '+port);
    if(cb){
      cb();
    }
  })
}
function stop(){
  app.stop()
}

module.exports = { start, stop };